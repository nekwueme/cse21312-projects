/*************************************
 * File name: Section01_RotateDLL.cc 
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu 
 * 
 * Given a set of inputs from the user, perform a 
 * set of rotations on a array of integers
 * 
 * This example is for a Doubly LinkedList only
 * ***********************************/

#include <iostream>
#include "Section01_DoublyLinkedList_NodeStruct.h"

const int INPUT_THROW = 100;

/*************************
 * Function Name: getInputConstr
 * Preconditions: int*, int*, char *
 * Postconditions: none 
 * Gets input from the user and determines the 
 * appropriate outputs. Also 
 * **********************/
void getInputConstr(int *length, int *rotations, char *cDirection){

    std::cout << "-----------------------------------------------------------------------" << std::endl;
    std::cout << "Enter the length, number of rotations, and the direction (L/R)." << std::endl;
    std::cout << "e.g.: length = 5, number of rotations = 4, and the direction is left..." << std::endl;
    std::cout << "Then the desired input is: 5 4 L" << std::endl;
    std::cout << "Enter your values: ";
    std::cin >> *length;
    
    // Check if it is an integer and if the length is greater than 0
    if(!(std::cin.good() && *length > 0)){
        std::cerr << "Incorrect first input. Must be an integer greater than 0." << std::endl;
        // Throw to the catch block in main 
        throw INPUT_THROW;
    }
    
    // Check if it is an integer and if the length is greater than 0
    std::cin >> *rotations;
    if(!(std::cin.good() && *length >= 0)){
        std::cerr << "Incorrect second input. Must be an integer >= 0." << std::endl;
        // Throw to the catch block in main 
        throw INPUT_THROW;
    }
    
    std::cin >> *cDirection;
    if(!(std::cin.good() && (*cDirection == 'R' || *cDirection == 'L'))){
        std::cerr << "Incorrect third input. Must be R or L." << std::endl;
        // Throw to the catch block in main 
        throw INPUT_THROW;
    }
}

/*************************
 * Function Name: getInputs
 * Preconditions: DLList<T>* myList, const int length
 * Postconditions: none 
 * Function reads in a set of inputs from the user, and - if 
 * they are of T* 
 * **********************/
template<class T>
void getInputs(DLList<T>* myList, const int length){
    std::cout << "Enter " << length << " integers: ";
    
    for(int i = 0; i < length; i++){
        
        // Initialize the template
        T temp;
        
        // Read in the variable 
        std::cin >> temp;
        
        // Since it is a template, use cin.good() to determine if the input was correct 
        if(!std::cin.good()){
            std::cerr << "Input " << i+1 << " is incorrect. Must be an integer." << std::endl;
            
            // Throw to the catch block in main 
            throw INPUT_THROW;
        }
        
        // Insert temp into the list 
        myList->insert(temp);
    }
}

/******************************
 * Function Name: clearSTDCIN
 * Preconditions: none 
 * Postconditions: none
 * 
 * Clears the std::cin buffer in the event 
 * that the user wishes to after an error.
 * ****************************/
 
 /*
void clearInStream(std::cin){
	std::cin.clear();
    std::cin.ignore(200, '\n');
}
*/

/******************************
 * Function Name: clearInStream
 * Preconditions: T& stream 
 * Postconditions: T& stream
 * 
 * Clears the input buffer in the event 
 * that the user wishes to after an error.
 * ****************************/
template<class T>
T& clearInStream(T& stream){
    stream.clear();
    stream.ignore(200, '\n');
    return stream;
}

/*************************
 * Function Name: main 
 * Preconditions: none 
 * Postconditions: int 
 * This is the main driver function 
 * **********************/
int main()
{
    char loop = 'y';
    
    while(loop == 'y'){
        try{
            // Create the Doubly LinkedList 
            DLList<int>* myList = new DLList<int>();
            
            // Set the Initial input variables
            int length, rotations;
            char cDirection;
            
            // Get the input constraints
            getInputConstr(&length, &rotations, &cDirection);

            // Clear the cin input buffer in the event there were more than 3 inputs 
            clearInStream(std::cin);
            
            // Get the inputs and add them to the array 
            getInputs(myList, length);

            // Clear the cin input buffer if there were more than 'length' inputs 
            clearInStream(std::cin);
            
            // Output the initial array 
            std::cout << "Initial Array is: " << myList << std::endl;
            
            // If the output 
            if(cDirection == 'R')
                rotateRight(myList, rotations % myList->getLength());
            else{ // Error checking already proved this must be 'L'
                rotateLeft(myList, rotations % myList->getLength());
            }
            
            std::cout << "After " << rotations << " rotations to the " << cDirection << " the Array is: " << myList << std::endl;
            
            delete myList;
        } catch(int INPUT_THROW){
            std::cout << "Exception caught" << std::endl;

            // Clear the cin input buffer
            clearInStream(std::cin);
        }
        
        
        std::cout << "Continue? Enter y/n: ";
        std::cin >> loop;
        // Clear the cin input buffer if there were more than one input 
        clearInStream(std::cin);
        
        while(!(std::cin.good() && (loop == 'y' || loop == 'n'))){
            std::cerr << "Incorrect. Must be y or n." << std::endl;
            std::cout << "Continue? Enter y/n: ";
            std::cin >> loop;
            // Clear the cin input buffer
            clearInStream(std::cin);
        }
    }
    
    std::cout << "...Exiting Program ..." << std::endl;
    return 0;
}